# -*- mode: perl6; coding: utf-8; -*-
use v6;

use Test;
use App::Brodie::Configuration;

plan 3;

chdir './t/';

my $config = App::Brodie::Configuration.load( '_config.yml' );

is './_posts', $config.source;
is './_site' , $config.destination;
is 'utf-8'   , $config.encoding;

done-testing;

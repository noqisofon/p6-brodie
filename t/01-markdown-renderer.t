# -*- mode: perl6; coding: utf-8; -*-
use v6;

use Test;
use Gumbo;
use App::Brodie::Renderer::MarkdownRenderer;

plan 2;

chdir './t/';

my $renderer      = App::Brodie::Renderer::MarkdownRenderer.new( 'hello.md' );

my $rendered-text = $renderer.render;

my $document      = parse-html( $rendered-text );

{
    my $h1 = $document.lookfor( :TAG<h1> ).first;

    is 'hello'                , $h1.contents.first;
}

{
    my $p  = $document.lookfor( :TAG<p> ).first;

    is 'こんにちは！こんにちは！', $p.contents.first;
}

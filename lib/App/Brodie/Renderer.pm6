use v6;

unit role App::Brodie::Renderer;


method render()           returns Str { ... }

method render-to($target)             { ... }

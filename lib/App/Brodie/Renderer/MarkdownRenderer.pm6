use v6;

use Text::Markdown::Discount;

use App::Brodie::Renderer;

unit class App::Brodie::Renderer::MarkdownRenderer does App::Brodie::Renderer;

has $.renderer is readonly;
has $.source   is readonly;

method new($path) {
    my $content  = slurp( $path );
    my $renderer = Text::Markdown::Discount.new( $content );
    
    self.bless( :$renderer, :source($path) );
}

method render() returns Str {
    $!renderer.render;
}

method render-to($target = $!source.IO.extension: '.html') {
    spurt $target, $!renderer.render;
}

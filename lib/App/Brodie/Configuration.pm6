use v6;

use YAML::Parser::LibYAML;

unit class App::Brodie::Configuration;

has Str  $.destination;
has Str  $.source;
has Bool $.safe;
has Str  @.exclude;
has Str  @.include;
has Str  $.timezone;
has Str  $.encoding;


submethod load($path) {
    my $config = yaml-parse( $path );

    App::Brodie::Configuration.new( |$config );
}
